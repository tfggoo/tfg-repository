using System;
using System.Diagnostics;
using System.Windows.Forms;
using WaveEngine.Adapter;
using System.Configuration;
using System.Collections.Specialized;

namespace GodsOfOlympus
{
    static class Program
    {
		[STAThread]
        static void Main()
        {
            using (App game = new App())
            {
                game.Run();
            }
        }
    }
}

