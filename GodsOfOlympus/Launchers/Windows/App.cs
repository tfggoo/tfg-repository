using GodsOfOlympus.Components;
using GodsOfOlympus.Scenes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Input;
using WaveEngine.Common.Math;
using WaveEngine.Components.Toolkit;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus
{
    public class App : WaveEngine.Adapter.Application
    {
            GodsOfOlympus.Game game;
        SpriteBatch spriteBatch;
        Texture2D splashScreen;
        bool splashState = true;
        TimeSpan time;
        Vector2 position;
        Color backgroundSplashColor;
        bool bloqued = false;
        public static string language;
        public static string savedGame;

        public App()
        {
            this.Width = 1280;
            this.Height = 720;
            this.FullScreen = false;
            this.WindowTitle = "GodsOfOlympus";
            this.HasVideoSupport = true;
            language = "en";
        }

        public override void Initialize()
        {
            this.game = new GodsOfOlympus.Game();
            this.game.Initialize(this);
            #region DEFAULT SPLASHSCREEN
            this.backgroundSplashColor = new Color("#ebebeb");
            this.spriteBatch = new SpriteBatch(WaveServices.GraphicsDevice);

            var resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            string name = string.Empty;

            foreach (string item in resourceNames)
            {
                if (item.Contains("SplashScreen.png"))
                {
                    name = item;
                    break;
                }
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new InvalidProgramException("License terms not agreed.");
            }

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
            {
                this.splashScreen = Texture2D.FromFile(WaveServices.GraphicsDevice, stream);
            }

            position = new Vector2();
            position.X = (this.Width / 2.0f) - (this.splashScreen.Width / 2.0f);
            position.Y = (this.Height / 2.0f) - (this.splashScreen.Height / 2.0f);
            #endregion
        }

        public override void Update(TimeSpan elapsedTime)
        {
            if (this.game != null && !this.game.HasExited)
            {
                if (WaveServices.Input.KeyboardState.F10 == ButtonState.Pressed)
                {
                    this.FullScreen = !this.FullScreen;
                }

                if (this.splashState)
                {
                    #region DEFAULT SPLASHSCREEN
                    this.time += elapsedTime;
                    if (time > TimeSpan.FromSeconds(2))
                    {
                        this.splashState = false;
                    }
                    #endregion
                }
                else
                {
                    if (WaveServices.Input.KeyboardState.Escape == ButtonState.Pressed)
                    {
                        WaveServices.Platform.Exit();
                    }
                    else
                    {
                        this.game.UpdateFrame(elapsedTime);
                    }
                }

                if(WaveServices.Input.KeyboardState.P == ButtonState.Pressed && !bloqued)
                {
                    Scene escena = null;
                    Entity camera = null;

                    if (WaveServices.ScreenContextManager.CurrentContext.FindScene<MyScene>()!=null)
                    {
                        escena = WaveServices.ScreenContextManager.CurrentContext.FindScene<MyScene>();
                    }
                    else if (WaveServices.ScreenContextManager.CurrentContext.FindScene<SecondLevelScene>()!=null)
                    {
                        escena = WaveServices.ScreenContextManager.CurrentContext.FindScene<SecondLevelScene>();
                        
                    }
                    else if (WaveServices.ScreenContextManager.CurrentContext.FindScene<ThirdLevelScene>() != null)
                    {
                        escena = WaveServices.ScreenContextManager.CurrentContext.FindScene<ThirdLevelScene>();

                    }
                    else if (WaveServices.ScreenContextManager.CurrentContext.FindScene<FourthLevelScene>() != null)
                    {
                        escena = WaveServices.ScreenContextManager.CurrentContext.FindScene<FourthLevelScene>();

                    }

                    if (escena!=null)
                    {
                        camera = escena.EntityManager.Find("Camera2D");

                        if (escena.IsPaused)
                        {
                            bloqued = true;
                            escena.Resume();
                            camera.FindChild("Pause").FindComponent<Transform2D>().Opacity = 0;
                            WaveServices.MusicPlayer.Resume();
                            var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(1), () =>
                            {
                                bloqued = false;
                            }, false);
                        }
                        else
                        {
                            bloqued = true;
                            escena.Pause();
                            camera.FindChild("Pause").FindComponent<Transform2D>().Opacity = 1;
                            camera.FindChild("Pause").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("pause_" + App.language);
                            WaveServices.MusicPlayer.Pause();
                            var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(1), () =>
                            {
                                bloqued = false;
                            }, false);
                        }
                    }
                }
            }
        }

        public override void Draw(TimeSpan elapsedTime)
        {
            if (this.game != null && !this.game.HasExited)
            {
                if (this.splashState)
                {
                    #region DEFAULT SPLASHSCREEN
                    WaveServices.GraphicsDevice.RenderTargets.SetRenderTarget(null);
                    WaveServices.GraphicsDevice.Clear(ref this.backgroundSplashColor, ClearFlags.Target, 1);
                    this.spriteBatch.Draw(this.splashScreen, this.position, Color.White);
                    this.spriteBatch.Render();
                    #endregion
                }
                else
                {
                    this.game.DrawFrame(elapsedTime);
                }
            }
        }

        /// <summary>
        /// Called when [activated].
        /// </summary>
        public override void OnActivated()
        {
            base.OnActivated();
            if (this.game != null)
            {
                game.OnActivated();
            }
        }

        /// <summary>
        /// Called when [deactivate].
        /// </summary>
        public override void OnDeactivate()
        {
            base.OnDeactivate();
            if (this.game != null)
            {
                game.OnDeactivated();
            }
        }
    }
}

