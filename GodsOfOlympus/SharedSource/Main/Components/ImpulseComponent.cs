﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Framework;

namespace GodsOfOlympus.Components
{
    [DataContract]
    class ImpulseComponent: Component
    {

        private float sideImpulse;
        private float jumpImpulse;

        [DataMember]
        public float SideImpulse
        {
            get
            {
                return this.sideImpulse;
            }
            set
            {
                if (value < 0.01)
                {
                    this.sideImpulse = 0.002f;
                }
                else
                {
                    this.sideImpulse = value;
                }
            }
        }

        [DataMember]
        public float JumpImpulse
        {
            get
            {
                return this.jumpImpulse;
            }
            set
            {
                this.jumpImpulse = value;
            }
        }

        protected override void ResolveDependencies()
        {
            base.ResolveDependencies();
        }
    }
}
