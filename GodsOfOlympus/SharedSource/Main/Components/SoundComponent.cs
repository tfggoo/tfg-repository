﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Media;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;
using WaveEngine.Framework.Sound;

namespace GodsOfOlympus.Components
{
    public enum SoundType
    {
        Coin = 0,
        Contact,
        Crash,
        CrateDrop,
        Jump,
        Life,
        Waterball,
        Watersplash,
        EagleDead,
        FrogDead,
        BeeDead,
        Menu,
        Count,
        FinishCount
    }
    [DataContract]
    class SoundComponent: Component
    {
        private SoundPlayer soundPlayer;
        private SoundBank bank;
        private Dictionary<SoundType, SoundInfo> sounds;


        protected override void Initialize()
        {
            base.Initialize();
            this.soundPlayer = WaveServices.SoundPlayer;

            // fill sound info
            sounds = new Dictionary<SoundType, SoundInfo>();
            sounds[SoundType.Coin] = new SoundInfo(WaveContent.Assets.Sounds.coin_wav);
            sounds[SoundType.Contact] = new SoundInfo(WaveContent.Assets.Sounds.contact_wav);
            sounds[SoundType.CrateDrop] = new SoundInfo(WaveContent.Assets.Sounds.crateDrop_wav);
            sounds[SoundType.Crash] = new SoundInfo(WaveContent.Assets.Sounds.crash_wav);
            sounds[SoundType.Jump] = new SoundInfo(WaveContent.Assets.Sounds.jump_wav);
            sounds[SoundType.Life] = new SoundInfo(WaveContent.Assets.Sounds.lifesound_wav);
            sounds[SoundType.Waterball] = new SoundInfo(WaveContent.Assets.Sounds.waterball_wav);
            sounds[SoundType.Watersplash] = new SoundInfo(WaveContent.Assets.Sounds.watersplash_wav);
            sounds[SoundType.EagleDead] = new SoundInfo(WaveContent.Assets.Sounds.eagleDead_wav);
            sounds[SoundType.FrogDead] = new SoundInfo(WaveContent.Assets.Sounds.frogDead_wav);
            sounds[SoundType.BeeDead] = new SoundInfo(WaveContent.Assets.Sounds.beeDead_wav);
            sounds[SoundType.Menu] = new SoundInfo(WaveContent.Assets.Sounds.Menu_wav);
            sounds[SoundType.Count] = new SoundInfo(WaveContent.Assets.Sounds.Count_wav);
            sounds[SoundType.FinishCount] = new SoundInfo(WaveContent.Assets.Sounds.FinishCount_wav);

            this.bank = new SoundBank(this.Assets);
            this.soundPlayer.RegisterSoundBank(bank);
            foreach (var item in this.sounds)
            {
                this.bank.Add(item.Value);
            }
        }

        public SoundInstance PlaySound(SoundType soundType, float volume = 1)
        {
            return this.soundPlayer.Play(this.sounds[soundType], volume);
        }
    }
}
