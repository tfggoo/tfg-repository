﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Components.Toolkit;
using WaveEngine.Framework;

namespace GodsOfOlympus.Components
{
    [DataContract]
    class CharacterAttributes: Component
    {
        private int lifes;
        private int coins;

        private TextComponent textComponentLifes;
        private TextComponent textComponentCoins;

        [DataMember]
        public int Lifes
        {
            get
            {
                return this.lifes;
            }
            set
            {
                this.lifes = value;

                if (this.isInitialized)
                {
                    this.UpdateTextLifes();
                }

            }
        }

        [DataMember]
        public int Coins
        {
            get
            {
                return this.coins;
            }
            set
            {
                this.coins = value;

                if (this.isInitialized)
                {
                    this.UpdateTextCoins();
                }

            }
        }

        protected override void ResolveDependencies()
        {
            base.ResolveDependencies();

            var textLifes = this.Owner.FindChild("lifes");
            this.textComponentLifes = textLifes.FindComponent<TextComponent>();

            var textCoins = this.Owner.FindChild("coins");
            this.textComponentCoins = textCoins.FindComponent<TextComponent>();
        }

        private void UpdateTextLifes()
        {
            this.textComponentLifes.Text = this.lifes.ToString();
        }


        private void UpdateTextCoins()
        {
            this.textComponentCoins.Text = this.coins.ToString();
        }
    }
}
