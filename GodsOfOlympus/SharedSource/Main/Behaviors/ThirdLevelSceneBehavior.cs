﻿using GodsOfOlympus.Components;
using GodsOfOlympus.Scenes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Math;
using WaveEngine.Components.Toolkit;
using WaveEngine.Components.Transitions;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Behaviors
{
    class ThirdLevelSceneBehavior : SceneBehavior
    {
        private Entity player;
        private List<Entity> coins;
        private List<Entity> specialCoins;
        private List<Entity> enemies;
        private List<Entity> lifesEntities;

        private List<Collider2D> colliders;
        private List<Collider2D> trapColliders;
        private List<Collider2D> coinColliders;
        private List<Collider2D> specialCoinColliders;
        private Collider2D endCollider;
        private List<Collider2D> enemiesColliders;
        private List<Collider2D> lifesColliders;

        private Vector2 initPlayerPosition;

        private SoundComponent soundComponent;

        private PlayerBehavior playerBehavior;
        bool isInitialized = false;
        bool winned = false;

        private List<Vector2> initCoinPositions;
        private List<Vector2> initSpecialCoinPositions;

        private ScreenTransition transition;
        private static readonly TimeSpan TRANSITIONTIME = new TimeSpan(0, 0, 0, 1, 0);

        protected override void ResolveDependencies()
        {
            
        }

        private void Initialize()
        {
            this.soundComponent = this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>();

            this.InitColliders();
            this.InitPlayer();
            this.InitEnd();
            this.InitTraps();
            this.InitCoins();
            this.InitSpecialCoins();
            this.InitEnemies();
            this.InitLifes();
        }

        private void InitColliders()
        {
            this.colliders = new List<Collider2D>();
            var result = this.Scene.EntityManager.FindAllByTag("collider");
            foreach (var collider in result)
            {
                Entity colliderEntity = collider as Entity;
                this.colliders.Add(colliderEntity.FindComponent<Collider2D>(false));
            }
        }

        private void InitPlayer()
        {
            this.player = this.Scene.EntityManager.Find("spriteSheet");
            this.playerBehavior = this.player.FindChild("Renderer").FindComponent<PlayerBehavior>();
            this.initPlayerPosition = this.player.FindComponent<Transform2D>().Position;
        }

        private void InitEnd()
        {
            var endEntity = this.Scene.EntityManager.Find("End");
            if (endEntity != null)
            {
                this.endCollider = endEntity.FindComponent<Collider2D>(false);
            }
        }

        private void InitTraps()
        {
            this.trapColliders = new List<Collider2D>();
            var traps = this.Scene.EntityManager.FindAllByTag("trap");
            foreach (var trap in traps)
            {
                Entity trapEntity = trap as Entity;
                this.trapColliders.Add(trapEntity.FindComponent<Collider2D>(false));
            }
        }

        private void InitCoins()
        {
            this.coins = new List<Entity>();
            this.coinColliders = new List<Collider2D>();
            this.initCoinPositions = new List<Vector2>();
            var result = this.Scene.EntityManager.FindAllByTag("coin");
            foreach (var coin in result)
            {
                Entity coinEntity = coin as Entity;

                this.coins.Add(coinEntity);
                this.coinColliders.Add(coinEntity.FindComponent<Collider2D>(false));
                this.initCoinPositions.Add(coinEntity.FindComponent<Transform2D>().Position);
            }
        }

        private void InitSpecialCoins()
        {
            this.specialCoins = new List<Entity>();
            this.specialCoinColliders = new List<Collider2D>();
            this.initSpecialCoinPositions = new List<Vector2>();
            var result = this.Scene.EntityManager.FindAllByTag("specialcoin");
            foreach (var specialCoin in result)
            {
                Entity specialCoinEntity = specialCoin as Entity;

                this.specialCoins.Add(specialCoinEntity);
                this.specialCoinColliders.Add(specialCoinEntity.FindComponent<Collider2D>(false));
                this.initSpecialCoinPositions.Add(specialCoinEntity.FindComponent<Transform2D>().Position);
            }
        }

        private void InitEnemies()
        {
            this.enemies = new List<Entity>();
            this.enemiesColliders = new List<Collider2D>();
            var result = this.Scene.EntityManager.FindAllByTag("Hole");
            foreach (var enemy in result)
            {
                Entity enemyEntity = enemy as Entity;

                enemyEntity.Tag = "Hole";
                this.enemies.Add(enemyEntity);
                this.enemiesColliders.Add(enemyEntity.FindComponent<Collider2D>(false));
            }
        }

        private void InitLifes()
        {
            this.lifesEntities = new List<Entity>();
            this.lifesColliders = new List<Collider2D>();
            var result = this.Scene.EntityManager.FindAllByTag("life");
            foreach (var life in result)
            {
                Entity lifeEntity = life as Entity;

                this.lifesEntities.Add(lifeEntity);
                this.lifesColliders.Add(lifeEntity.FindComponent<Collider2D>(false));
            }
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!this.isInitialized)
            {
                this.Initialize();
                this.isInitialized = true;
            }

            this.CheckEnd();
            this.CheckTraps();
            this.CheckCoins();
            this.CheckSpecialCoins();
            this.CheckAttackEnemies();
            this.CheckLifes();
            this.CheckAttacks();
        }

        private void CheckEnd()
        {
            if (this.playerBehavior.Collider.Intersects(this.endCollider))
            {
                this.Win();
            }

        }

        private void Win()
        {
            if (!winned)
            {
                winned = true;
                this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);
                WinScene winScene = new WinScene();
                winScene.Level = "3";
                winScene.Coins = this.Scene.EntityManager.Find("Camera2D").FindChild("coins").FindComponent<TextComponent>().Text;
                winScene.CollectedCoins = System.IO.File.ReadAllLines(App.savedGame)[0].Split(':')[1];
                this.saveGame();
                var context = new ScreenContext(winScene);
                WaveServices.ScreenContextManager.To(context, transition);
            }

        }

        private void CheckTraps()
        {
            for (int i = trapColliders.Count - 1; i >= 0; i--)
            {
                Collider2D trapCollider = this.trapColliders[i];
                if (this.playerBehavior.Collider.Intersects(trapCollider))
                {
                    this.Defeat();
                }
            }
        }

        private void Defeat()
        {
            this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Lifes -= 1;
            this.CheckNumberLifes();
            this.soundComponent.PlaySound(SoundType.Crash);
            this.ResetGame();
            if (this.playerBehavior.waterball != null)
            {
                this.Scene.EntityManager.Remove(this.playerBehavior.waterball);
                this.playerBehavior.waterball = null;
            }
        }

        private void CheckLifes()
        {
            for (int i = lifesColliders.Count - 1; i >= 0; i--)
            {
                Collider2D lifeCollider = this.lifesColliders[i];
                if (lifeCollider.Owner.Enabled && this.playerBehavior.Collider.Intersects(lifeCollider))
                {
                    this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Lifes += 1;
                    lifeCollider.Owner.Enabled = false;
                    this.soundComponent.PlaySound(SoundType.Life);
                }
            }
        }

        private void CheckNumberLifes()
        {
            if (this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Lifes == 0)
            {
                this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);

                var context = new ScreenContext(new GameOverScene());
                WaveServices.ScreenContextManager.To(context, transition);
            }
        }

        private void ResetGame()
        {
            this.playerBehavior.Reset();
            this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Coins = 0;
            foreach (var coin in this.coins)
            {
                coin.Enabled = true;
            }

            foreach (var specialCoin in this.specialCoins)
            {
                specialCoin.Enabled = true;
            }
        }

        private void CheckCoins()
        {
            for (int i = coinColliders.Count - 1; i >= 0; i--)
            {
                Collider2D coinCollider = this.coinColliders[i];
                if (coinCollider.Owner.Enabled && this.playerBehavior.Collider.Intersects(coinCollider))
                {
                    this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Coins += 1;
                    coinCollider.Owner.Enabled = false;
                    this.soundComponent.PlaySound(SoundType.Coin);
                }
            }
        }

        private void CheckSpecialCoins()
        {
            for (int i = specialCoinColliders.Count - 1; i >= 0; i--)
            {
                Collider2D specialCoinCollider = this.specialCoinColliders[i];
                if (specialCoinCollider.Owner.Enabled && this.playerBehavior.Collider.Intersects(specialCoinCollider))
                {
                    this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Coins += 10;
                    specialCoinCollider.Owner.Enabled = false;
                    this.soundComponent.PlaySound(SoundType.Coin);
                }
            }
        }

        private void CheckAttackEnemies()
        {
            for (int i = enemies.Count - 1; i >= 0; i--)
            {
                if(this.enemies[i].FindComponent<HoleBehavior>().waterball!=null)
                {
                    CircleCollider2D attackEnemy = this.enemies[i].FindComponent<HoleBehavior>().waterball.FindComponent<CircleCollider2D>();
                    if (this.playerBehavior.Collider.Intersects(attackEnemy))
                    {
                        this.Defeat();
                        this.Scene.EntityManager.Remove(this.enemies[i].FindComponent<HoleBehavior>().waterball);
                        this.enemies[i].FindComponent<HoleBehavior>().waterball = null;
                        this.soundComponent.PlaySound(SoundType.Watersplash);
                    }
                    else
                    {
                        for (int j = colliders.Count - 1; j >= 0; j--)
                        {
                            if (attackEnemy.Intersects(colliders[j]))
                            {
                                this.Scene.EntityManager.Remove(this.enemies[i].FindComponent<HoleBehavior>().waterball);
                                this.enemies[i].FindComponent<HoleBehavior>().waterball = null;
                            }
                        }
                    }
                }
            }
        }

        private void CheckAttacks()
        {
            if (playerBehavior.waterball != null)
            {
                var waterballCollider = playerBehavior.waterball.FindComponent<CircleCollider2D>();
                for (int i = enemies.Count - 1; i >= 0; i--)
                {
                    Collider2D enemy = this.enemiesColliders[i];
                    if (waterballCollider.Intersects(enemy) && enemy.Owner.Enabled && enemy.Owner.Tag!="Hole")
                    {
                        this.DefeatEnemy(i);
                    }
                }

                if (playerBehavior.waterball != null)
                {
                    for (int i = colliders.Count - 1; i >= 0; i--)
                    {
                        if (waterballCollider.Intersects(colliders[i]) && colliders[i].Owner.Enabled)
                        {
                            this.Scene.EntityManager.Remove(this.playerBehavior.waterball);
                            this.playerBehavior.waterball = null;
                            this.soundComponent.PlaySound(SoundType.Watersplash);
                        }
                    }
                }

            }
        }

        private void DefeatEnemy(int i)
        {
            this.enemies[i].FindComponent<RectangleCollider2D>().Owner.Enabled = false;
            this.Scene.EntityManager.Remove(this.playerBehavior.waterball);
            this.playerBehavior.waterball = null;
            this.soundComponent.PlaySound(SoundType.Watersplash);
        }

        private void saveGame()
        {
            DateTime fecha = DateTime.Now;
            int CollectedCoins = Int32.Parse(System.IO.File.ReadAllLines(App.savedGame)[0].Split(':')[1]) + this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Coins;
            File.Delete(App.savedGame);

            using (StreamWriter file = File.CreateText(App.savedGame))
            {
                file.AutoFlush = true;
                file.WriteLine("Coins: " + CollectedCoins);
                file.WriteLine("Lifes: " + this.Scene.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Lifes);
                file.WriteLine("Level: 4");
                file.Close();
                file.Dispose();
            }
        }
    }
}
