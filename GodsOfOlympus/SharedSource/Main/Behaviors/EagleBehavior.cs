﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Math;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;

namespace GodsOfOlympus.Behaviors
{
    [DataContract]
    class EagleBehavior : Behavior
    {

        [RequiredComponent]
        public Transform2D Transform;

        private const float Speed = 50;
        private Vector2 initPosition;


        private AnimState currentState;
        private enum AnimState {Up, Down};

        public EagleBehavior()
        {
            this.Transform = null;
        }

        protected override void Initialize()
        {
            base.Initialize();
            
            this.initPosition = this.Transform.Position;
        }
        protected override void Update(TimeSpan gameTime)
        {
            var localPosition = this.Transform.LocalPosition;

            switch (currentState)
            {
                case AnimState.Up:
                    if (this.initPosition.Y + 50 <= localPosition.Y)
                    {
                        currentState = AnimState.Down;
                    }
                    localPosition.Y += (float)(Speed * gameTime.TotalSeconds);
                    break;

                case AnimState.Down:
                    if (this.initPosition.Y - 50 >= localPosition.Y)
                    {
                        currentState = AnimState.Up;
                    }
                    localPosition.Y -= (float)(Speed * gameTime.TotalSeconds);
                    break;
            }

            this.Transform.LocalPosition = localPosition;

        }
    }
}
