﻿using GodsOfOlympus.Scenes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using WaveEngine.Common.Graphics;
using WaveEngine.Components.Toolkit;
using WaveEngine.Components.Transitions;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Behaviors
{
    class MapSceneBehavior : SceneBehavior
    {

        private ScreenTransition transition;
        private static readonly TimeSpan TRANSITIONTIME = new TimeSpan(0, 0, 0, 2, 0);
        private bool isInitialized = false;
        private string levelMap;

        public MapSceneBehavior(string level)
        {
            this.levelMap = level;
        }
        

        protected override void ResolveDependencies()
        {
            
        }

        private void Initialize()
        {

            if (this.levelMap.Contains("1"))
            {
                this.Scene.EntityManager.Find("text").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("firstLevel_" + App.language);
            }
            else if(this.levelMap.Contains("2"))
            {
                this.Scene.EntityManager.Find("text").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("secondLevel_" + App.language);
            }
            else if (this.levelMap.Contains("3"))
            {
                this.Scene.EntityManager.Find("text").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("thirdLevel_" + App.language);
            }
            else if (this.levelMap.Contains("4"))
            {
                this.Scene.EntityManager.Find("text").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("fourthLevel_" + App.language);
            }

            var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(3), () =>
            {
                this.transition = new DoorwayTransition(TRANSITIONTIME);

                if (levelMap.Contains("1"))
                {
                    var context = new ScreenContext(new MyScene());
                    WaveServices.ScreenContextManager.To(context, transition);
                }
                else if(levelMap.Contains("2"))
                {
                    var context = new ScreenContext(new SecondLevelScene());
                    WaveServices.ScreenContextManager.To(context, transition);
                }
                else if (levelMap.Contains("3"))
                {
                    var context = new ScreenContext(new ThirdLevelScene());
                    WaveServices.ScreenContextManager.To(context, transition);
                }
                else if (levelMap.Contains("4"))
                {
                    var context = new ScreenContext(new FourthLevelScene());
                    WaveServices.ScreenContextManager.To(context, transition);
                }



            }, false);
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!this.isInitialized)
            {
                this.isInitialized = true;
                this.Initialize();
            }
        }
    }
}
