﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Math;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;

namespace GodsOfOlympus.Behaviors
{
    [DataContract]
    class HoleBehavior : Behavior
    {
        [RequiredComponent]
        public Transform2D Transform;

        private AnimState currentState, lastState;
        private enum AnimState { Shooting, Stay};

        private bool isInitialized = false;
        DateTime time;
        public Entity waterball;
        private string direction;
        private int speedAttack = 80;

        public HoleBehavior(string direction)
        {
            this.currentState = AnimState.Stay;
            this.lastState = AnimState.Stay;
            this.direction = direction;
        }

        protected override void Initialize()
        {
            base.Initialize();

            time = DateTime.Now;
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!isInitialized)
            {
                this.isInitialized = true;
                this.Initialize();
            }
            if (currentState != AnimState.Shooting)
            {
                DateTime aux = DateTime.Now;
                TimeSpan result = aux.Subtract(time);
                int aux2 = result.Seconds;
                if (result.Seconds >= 3)
                {
                    WaveEngine.Framework.Services.Random r = new WaveEngine.Framework.Services.Random();
                    if (r.NextDouble() < 0.5)
                    {
                        this.lastState = this.currentState;
                        this.currentState = AnimState.Shooting;
                    }
                    else
                    {
                        this.lastState = this.currentState;
                        this.currentState = AnimState.Stay;
                    }

                    time = DateTime.Now;
                }

                if (currentState != lastState)
                {
                    switch (currentState)
                    {
                        case AnimState.Stay:
                            lastState = currentState;
                            break;
                        case AnimState.Shooting:
                            this.Shoot(gameTime);
                            break;
                    }
                }
            }

            if (waterball != null)
            {
                int directionAttack = 1;

                if(direction=="1" || direction == "4")
                {
                    directionAttack = -1;
                }


                if(direction=="2" || direction == "4")
                {
                    var localPosition = this.waterball.FindComponent<Transform2D>().LocalPosition;

                    localPosition.X += (float)(speedAttack * gameTime.TotalSeconds * directionAttack);


                    this.waterball.FindComponent<Transform2D>().LocalPosition = localPosition;
                    this.waterball.FindComponent<Transform2D>().LocalRotation += 0.3F;
                }
                else
                {
                    var localPosition = this.waterball.FindComponent<Transform2D>().LocalPosition;

                    localPosition.Y += (float)(speedAttack * gameTime.TotalSeconds * directionAttack);


                    this.waterball.FindComponent<Transform2D>().LocalPosition = localPosition;
                    this.waterball.FindComponent<Transform2D>().LocalRotation += 0.3F;
                }
                
            }
            else
            {
                this.lastState = this.currentState;
                this.currentState = AnimState.Stay;
            }
        }

        public void Shoot(TimeSpan gameTime)
        {
            int x = 0;
            int y = 0;
            if (this.direction == "1")
            {
                y = 10;
            }
            else if(this.direction == "2"){
                x = 10;
            }
            else if (this.direction == "3")
            {
                y = -10;
            }
            else
            {
                x = -10;
            }

            Entity crateEntity = new Entity() { Tag = "waterball" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(Transform.X + x, Transform.Y + y), Origin = Vector2.Center, DrawOrder = -100, Scale = new Vector2(0.6F, 0.6F) })
                .AddComponent(new Sprite(WaveContent.Assets.Textures.waterball_png))
                //.AddComponent(new SpriteRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteRenderer())
                .AddComponent(new CircleCollider2D())
                ;
            this.waterball = crateEntity;
            this.EntityManager.Add(crateEntity);
        }
    }
}
