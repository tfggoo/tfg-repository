﻿using GodsOfOlympus.Components;
using GodsOfOlympus.Scenes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Media;
using WaveEngine.Components.Toolkit;
using WaveEngine.Components.Transitions;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Behaviors
{
    class FinishSceneBehavior : SceneBehavior
    {

        private ScreenTransition transition;
        private static readonly TimeSpan TRANSITIONTIME = new TimeSpan(0, 0, 0, 1, 0);
        bool isInitialized = false;
        private string levelMap;
        private string coins;
        private string collectedCoins;
        private bool bloqued, start;
        protected override void ResolveDependencies()
        {
            
        }

        public FinishSceneBehavior(string level, string coins, string collectedCoins)
        {
            this.levelMap = level;
            this.coins = coins;
            this.collectedCoins = collectedCoins;
        }

        private void Initialize()
        {
            if(this.Scene.Name == "WinScene")
            {
                WaveServices.MusicPlayer.Stop();
                MusicInfo musicInfo = new MusicInfo(WaveContent.Assets.Sounds.Victory_mp3);
                WaveServices.MusicPlayer.Volume = 0.6f;
                WaveServices.MusicPlayer.Play(musicInfo);

                this.Scene.EntityManager.Find("Win").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("congra_" + App.language);
                this.Scene.EntityManager.Find("CollectedCoinsText").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("collectedCoins_" + App.language);
                this.Scene.EntityManager.Find("CoinsText").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("coinsLevel_" + App.language);

                if (levelMap != "1")
                {
                    this.Scene.EntityManager.Find("Action").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("actionFin_" + App.language);
                    this.Scene.EntityManager.Find("Coins").FindComponent<TextComponent>().Text = this.coins;
                    this.Scene.EntityManager.Find("CollectedCoins").FindComponent<TextComponent>().Text = this.collectedCoins;
                }
                else
                {
                    this.Scene.EntityManager.Find("Action").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("actionCon_" + App.language);
                    this.Scene.EntityManager.Find("Coins").FindComponent<TextComponent>().Text = this.coins;
                    this.Scene.EntityManager.Find("CollectedCoins").FindComponent<TextComponent>().Text = "0";
                }

                bloqued = true;
                start = false;
                this.Scene.EntityManager.Find("Action").FindComponent<TextRenderer2D>().IsVisible = false;
                this.Scene.EntityManager.Find("Saved").FindComponent<TextRenderer2D>().IsVisible = false;

                var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(2), () =>
                {
                    start = true;
                }, false);
            }
            else
            {
                WaveServices.MusicPlayer.Stop();
                MusicInfo musicInfo = new MusicInfo(WaveContent.Assets.Sounds.continue_mp3);
                WaveServices.MusicPlayer.Volume = 0.6f;
                WaveServices.MusicPlayer.Play(musicInfo);
            }
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!this.isInitialized)
            {
                this.Initialize();
                this.isInitialized = true;
            }

            

            var input = WaveServices.Input.KeyboardState;

            if (bloqued)
            {
                int coins = Int32.Parse(this.Scene.EntityManager.Find("Coins").FindComponent<TextComponent>().Text);
                int collectedCoins = Int32.Parse(this.Scene.EntityManager.Find("CollectedCoins").FindComponent<TextComponent>().Text);

                if (coins != 0)
                {
                    if (start)
                    {
                        collectedCoins++;
                        coins--;
                        
                        this.Scene.EntityManager.Find("Coins").FindComponent<TextComponent>().Text = coins.ToString();
                        this.Scene.EntityManager.Find("Coins").FindComponent<TextRenderer2D>().Draw(gameTime);
                        this.Scene.EntityManager.Find("CollectedCoins").FindComponent<TextComponent>().Text = collectedCoins.ToString();
                        this.Scene.EntityManager.Find("CollectedCoins").FindComponent<TextRenderer2D>().Draw(gameTime);
                        this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Count);

                        start = false;
                        var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.01), () =>
                        {
                            start = true;
                        }, false);
                    }
                }
                else
                {
                    bloqued = false;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.FinishCount);
                    this.Scene.EntityManager.Find("Action").FindComponent<TextRenderer2D>().IsVisible = true;
                    if (levelMap!="4")
                    {
                        this.Scene.EntityManager.Find("Saved").FindComponent<TextRenderer2D>().IsVisible = true;
                    }
                }
            }
            else if (input.Enter == WaveEngine.Common.Input.ButtonState.Pressed && !bloqued)
            {
                
                this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);

                if (levelMap=="1")
                {
                    MapScene map = new Scenes.MapScene();
                    map.Level = "2";
                    var context = new ScreenContext(map);
                    WaveServices.ScreenContextManager.To(context, transition);
                }
                else if (levelMap == "2")
                {
                    MapScene map = new Scenes.MapScene();
                    map.Level = "3";
                    var context = new ScreenContext(map);
                    WaveServices.ScreenContextManager.To(context, transition);
                }
                else if (levelMap == "3")
                {
                    MapScene map = new Scenes.MapScene();
                    map.Level = "4";
                    var context = new ScreenContext(map);
                    WaveServices.ScreenContextManager.To(context, transition);
                }
                else
                {
                    var context = new ScreenContext(new MainScene());
                    WaveServices.ScreenContextManager.To(context, transition);
                }
               
            }
        }

        
    }
}
