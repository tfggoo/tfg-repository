﻿using GodsOfOlympus.Components;
using GodsOfOlympus.Scenes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Input;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Components.Toolkit;
using WaveEngine.Components.Transitions;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Behaviors
{
    public class PresentationSceneBehavior : SceneBehavior
    {
        private ScreenTransition transition;
        private static readonly TimeSpan TRANSITIONTIME = new TimeSpan(0, 0, 0, 1, 0);
        private bool isInitialized = false;
        private Timer timerDeclaration;
        private bool bloqued = true;
        private string language;

        private void Initialize()
        {

            var timer1 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(2), () =>
            {
                var timer11 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromMilliseconds(50), () =>
                {
                    if (this.Scene.EntityManager.Find("text").FindComponent<TextComponent>().Alpha!=0)
                    {
                        this.Scene.EntityManager.Find("text").FindComponent<TextComponent>().Alpha -= 0.1f;
                    }

                }, true);
                timerDeclaration = timer11;
            }, false);

            var timer2 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(4), () =>
            {
                timerDeclaration.Pause();
                var timer22 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromMilliseconds(50), () =>
                {
                    if (this.Scene.EntityManager.Find("text01").FindComponent<TextComponent>().Alpha != 1)
                    {
                        this.Scene.EntityManager.Find("text01").FindComponent<TextComponent>().Alpha += 0.1f;
                    }
                    
                }, true);
                timerDeclaration = timer22;
            }, false);

            var timer3 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(6), () =>
            {
                timerDeclaration.Pause();
                var timer33 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromMilliseconds(50), () =>
                {
                    if (this.Scene.EntityManager.Find("text01").FindComponent<TextComponent>().Alpha != 0)
                    {
                        this.Scene.EntityManager.Find("text01").FindComponent<TextComponent>().Alpha -= 0.1f;
                    }

                }, true);
                timerDeclaration = timer33;
            }, false);

            var timer4 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(9), () =>
            {
                timerDeclaration.Pause();
                var timer44 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromMilliseconds(50), () =>
                {
                    if (this.Scene.EntityManager.Find("Seleccion").FindComponent<TextComponent>().Alpha != 1)
                    {
                        this.Scene.EntityManager.Find("Seleccion").FindComponent<TextComponent>().Alpha += 0.1f;
                        this.Scene.EntityManager.Find("Español").FindComponent<TextComponent>().Alpha += 0.1f;
                        this.Scene.EntityManager.Find("English").FindComponent<TextComponent>().Alpha += 0.1f;
                    }

                    if (this.Scene.EntityManager.Find("Seleccion").FindComponent<TextComponent>().Alpha >= 1)
                    {
                        if (!timerDeclaration.IsPaused)
                        {
                            this.Scene.EntityManager.Find("SelectSpanish").FindComponent<SpriteRenderer>().IsVisible = true;
                            bloqued = false;
                            language = "es";
                            timerDeclaration.Pause();
                        }
                    }

                }, true);
                timerDeclaration = timer44;
            }, false);
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!this.isInitialized)
            {
                this.isInitialized = true;
                this.Initialize();                
            }
            

            if (!bloqued)
            {
                if(WaveServices.Input.KeyboardState.Enter == ButtonState.Pressed && !bloqued)
                {
                    bloqued = true;
                    App.language = this.language;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                    this.ResetText();

                    var timer5 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0), () =>
                    {
                        var timer55 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromMilliseconds(50), () =>
                        {
                            if (this.Scene.EntityManager.Find("Seleccion").FindComponent<TextComponent>().Alpha != 0)
                            {
                                this.Scene.EntityManager.Find("Seleccion").FindComponent<TextComponent>().Alpha -= 0.1f;
                                this.Scene.EntityManager.Find("Español").FindComponent<TextComponent>().Alpha -= 0.1f;
                                this.Scene.EntityManager.Find("English").FindComponent<TextComponent>().Alpha -= 0.1f;
                            }

                        }, true);

                        this.Scene.EntityManager.Find("SelectSpanish").FindComponent<SpriteRenderer>().IsVisible = false;
                        this.Scene.EntityManager.Find("SelectEnglish").FindComponent<SpriteRenderer>().IsVisible = false;
                        timerDeclaration = timer55;
                    }, false);

                    var timer6 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(2), () =>
                    {
                        timerDeclaration.Pause();
                        this.Scene.EntityManager.Find("Controls").FindComponent<Transform2D>().Opacity = 0.0f;
                        var timer66 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromMilliseconds(50), () =>
                        {
                            if (this.Scene.EntityManager.Find("Controls").FindComponent<Transform2D>().Opacity < 0.9f)
                            {
                                this.Scene.EntityManager.Find("Controls").FindComponent<Transform2D>().Opacity += 0.1f;
                            }

                        }, true);
                        timerDeclaration = timer66;
                    }, false);

                    var timer7 = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(7), () =>
                    {
                        this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);

                        var context = new ScreenContext(new MainScene());
                        timerDeclaration.Pause();
                        WaveServices.ScreenContextManager.To(context, transition);

                    }, false);
                }
                else if ((WaveServices.Input.KeyboardState.Up == ButtonState.Pressed || WaveServices.Input.KeyboardState.Down == ButtonState.Pressed) && !bloqued)
                {
                    if (language == "es")
                    {
                        language = "en";
                        this.Scene.EntityManager.Find("SelectSpanish").FindComponent<SpriteRenderer>().IsVisible = false;
                        this.Scene.EntityManager.Find("SelectEnglish").FindComponent<SpriteRenderer>().IsVisible = true;
                        this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                    }
                    else
                    {
                        language = "es";
                        this.Scene.EntityManager.Find("SelectSpanish").FindComponent<SpriteRenderer>().IsVisible = true;
                        this.Scene.EntityManager.Find("SelectEnglish").FindComponent<SpriteRenderer>().IsVisible = false;
                        this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                    }

                    bloqued = true;

                    var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
                    {
                        bloqued = false;
                    }, false);
                }
            }

            

        }

        protected override void ResolveDependencies()
        {
            
        }

        private void ResetText()
        {
            Entity controls = this.Scene.EntityManager.Find("Controls");
            controls.FindChild("Title").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("controls_" + App.language);
            controls.FindChild("Up").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("up_" + App.language);
            controls.FindChild("Up_Description").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("up_description_" + App.language);
            controls.FindChild("Down").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("down_" + App.language);
            controls.FindChild("Down_Description").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("down_description_" + App.language);
            controls.FindChild("Right").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("right_" + App.language);
            controls.FindChild("Right_Description").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("right_description_" + App.language);
            controls.FindChild("Left").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("left_" + App.language);
            controls.FindChild("Left_Description").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("left_description_" + App.language);
            controls.FindChild("E").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("e_" + App.language);
            controls.FindChild("E_Description").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("e_description_" + App.language);
            controls.FindChild("P").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("p_" + App.language);
            controls.FindChild("P_Description").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("p_description_" + App.language);
            controls.FindChild("Space").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("space_" + App.language);
            controls.FindChild("Space_Description").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("space_description_" + App.language);
        }
    }
}
