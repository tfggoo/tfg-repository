﻿using GodsOfOlympus.Components;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Math;
using WaveEngine.Components.Animation;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Diagnostic;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Managers;
using WaveEngine.Framework.Physics2D;
using WaveEngine.Framework.Services;
using WaveEngine.Common.Physics2D;

namespace GodsOfOlympus.Behaviors
{
    [DataContract]
    class PlayerBehavior: Behavior
    {
        private ImpulseComponent impulses;
        private Input input;
        private bool isJump;
        private int collisionCounter = 0;
        private Vector2 initPosition;

        private SoundComponent soundManager;
        private VirtualScreenManager vm;

        public Entity waterball;
        private int speedAttack = 80;
        private int directionAttack;

        private bool OnFloor
        {
            get
            {
                Labels.Add("CollisionCounter", collisionCounter);
                return this.collisionCounter > 0;
            }
        }


        
        public RigidBody2D RigidBody;

        [RequiredComponent]
        public Transform2D Transform2D;
        
        public Collider2D Collider;

        [RequiredComponent]
        public Animation2D Animation;

        private enum AnimState { Stay, Right, Left };
        private AnimState currentState, lastState;

        public PlayerBehavior()
        {
            this.Animation = null;
            this.Transform2D = null;
            this.RigidBody = null;
            this.currentState = AnimState.Stay;
            this.waterball = null;
        }

        protected override void Initialize()
        {
            base.Initialize();
            this.soundManager = this.EntityManager.Find("SoundManager").FindComponent<SoundComponent>();
            this.impulses = this.Owner.FindComponent<ImpulseComponent>();
            this.vm = this.Owner.Scene.VirtualScreenManager;

            this.input = WaveServices.Input;
            this.initPosition = this.Transform2D.Position;
            this.currentState = AnimState.Stay;
            this.RigidBody = this.Owner.FindComponentInParents<RigidBody2D>();
            this.Collider = this.Owner.FindComponentInParents<RectangleCollider2D>();

            this.Collider.BeginCollision += Collider_BeginCollision;
            this.Collider.EndCollision += Collider_EndCollision;
        }

        private void Collider_EndCollision(ICollisionInfo2D contact)
        {
            this.collisionCounter--;
        }

        private void Collider_BeginCollision(ICollisionInfo2D contact)
        {
            this.collisionCounter++;
        }

        protected override void Update(TimeSpan gameTime)
        {
            this.HandleKeys();

            if (waterball != null)
            {
                var localPosition = this.waterball.FindComponent<Transform2D>().LocalPosition;
                
                localPosition.X += (float)(speedAttack * gameTime.TotalSeconds*directionAttack);
                
                    
                this.waterball.FindComponent<Transform2D>().LocalPosition = localPosition;
                this.waterball.FindComponent<Transform2D>().LocalRotation += 0.3F;
            }

        }

        private void HandleKeys()
        {
            if (this.input.KeyboardState.IsConnected)
            {
                var input = WaveServices.Input.KeyboardState;

                if (input.D == WaveEngine.Common.Input.ButtonState.Pressed || input.Right == WaveEngine.Common.Input.ButtonState.Pressed)
                {
                    this.MoveRight();
                    currentState = AnimState.Right;
                }

                if (input.A == WaveEngine.Common.Input.ButtonState.Pressed || input.Left == WaveEngine.Common.Input.ButtonState.Pressed)
                {
                    this.MoveLeft();
                    currentState = AnimState.Left;
                }

                if (input.E == WaveEngine.Common.Input.ButtonState.Pressed && this.waterball==null)
                {
                    this.Attack();
                }

                if (input.Space == WaveEngine.Common.Input.ButtonState.Pressed)
                {
                    this.Jump();
                    this.isJump = true;
                }
                else
                {
                    this.isJump = false;                    
                }


                if (input.A == WaveEngine.Common.Input.ButtonState.Released && input.D == WaveEngine.Common.Input.ButtonState.Released 
                    && input.Right == WaveEngine.Common.Input.ButtonState.Released && input.Left == WaveEngine.Common.Input.ButtonState.Released)
                {
                    currentState = AnimState.Stay;
                }

                if (currentState != lastState)
                {
                    switch (currentState)
                    {
                        case AnimState.Stay:
                            Animation.PlayAnimation("Stay", true);
                            break;
                        case AnimState.Right:
                            Animation.PlayAnimation("Running", true);
                            this.Transform2D.Effect = WaveEngine.Common.Graphics.SpriteEffects.None;
                            break;
                        case AnimState.Left:
                            Animation.PlayAnimation("Running", true);
                            this.Transform2D.Effect = WaveEngine.Common.Graphics.SpriteEffects.FlipHorizontally;
                            break;
                    }
                }

                lastState = currentState;

            }
        }

        private void Jump()
        {
            if (!this.isJump && this.OnFloor)
            {
                this.RigidBody.ApplyLinearImpulse(Vector2.UnitY * -impulses.JumpImpulse, this.RigidBody.Transform2D.Position);
                this.soundManager.PlaySound(SoundType.Jump);
            }
        }

        private void MoveLeft()
        {
            if (this.RigidBody.LinearVelocity.X > -200)
            {
                this.RigidBody.ApplyLinearImpulse(Vector2.UnitX * -impulses.SideImpulse, this.RigidBody.Transform2D.Position);
            }
        }

        private void MoveRight()
        {
            if (this.RigidBody.LinearVelocity.X<200)
            {
                this.RigidBody.ApplyLinearImpulse(Vector2.UnitX * impulses.SideImpulse, this.RigidBody.Transform2D.Position);
            }
        }

        public void MoveUp()
        {
            if (this.RigidBody.LinearVelocity.Y > -100)
            {
                this.RigidBody.ApplyLinearImpulse(Vector2.UnitY * -impulses.SideImpulse, this.RigidBody.Transform2D.Position);
            }
        }

        public void MoveDown()
        {
            if (this.RigidBody.LinearVelocity.Y < 100)
            {
                this.RigidBody.ApplyLinearImpulse(Vector2.UnitY * impulses.SideImpulse, this.RigidBody.Transform2D.Position);
            } 
        }

        public void Reset()
        {
            this.RigidBody.ResetPosition(this.initPosition);
            this.RigidBody.Transform2D.Rotation = 0;
            this.collisionCounter = 0;
        }

        public void Attack()
        {
            if (this.Transform2D.Effect != WaveEngine.Common.Graphics.SpriteEffects.FlipHorizontally)
            {
                Entity crateEntity = new Entity("waterball") { Tag = "waterball" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(Transform2D.X + 10, Transform2D.Y), Origin = Vector2.Center, DrawOrder = -100, Scale = new Vector2(0.4F, 0.4F) })
                .AddComponent(new Sprite(WaveContent.Assets.Textures.waterball_png))
                //.AddComponent(new SpriteRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteRenderer())
                .AddComponent(new CircleCollider2D())
                ;
                this.waterball = crateEntity;
                this.EntityManager.Add(crateEntity);
                this.soundManager.PlaySound(SoundType.Waterball);
                directionAttack = 1;
            }
            else
            {
                Entity crateEntity = new Entity("waterball") { Tag = "waterball" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(Transform2D.X - 10, Transform2D.Y), Origin = Vector2.Center, DrawOrder = -100, Scale = new Vector2(0.4F, 0.4F) })
                .AddComponent(new Sprite(WaveContent.Assets.Textures.waterball_png))
                //.AddComponent(new SpriteRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteRenderer())
                .AddComponent(new CircleCollider2D())
                ;
                this.waterball = crateEntity;
                this.EntityManager.Add(crateEntity);
                this.soundManager.PlaySound(SoundType.Waterball);
                directionAttack = -1;
            }
            
        }

        
    }
}
