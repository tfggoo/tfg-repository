﻿using GodsOfOlympus.Components;
using GodsOfOlympus.Scenes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Input;
using WaveEngine.Common.Math;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Components.Toolkit;
using WaveEngine.Components.Transitions;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Behaviors
{
    class LoadGameSceneBehavior : SceneBehavior
    {
        private bool isInitialized = false;
        private ScreenTransition transition;
        private static readonly TimeSpan TRANSITIONTIME = new TimeSpan(0, 0, 0, 2, 0);
        private bool bloqued;
        private int position;
        private int maxFiles;

        protected override void ResolveDependencies()
        {
            
        }

        private void Initialize()
        {
            this.Scene.EntityManager.Find("Title").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("loadGame_" + App.language);

            int i = 0;
            maxFiles = 0;
            DirectoryInfo di = new DirectoryInfo("../../../../../Content/savedGames");
            foreach (FileInfo file in di.GetFiles())
            {
                if (!file.Name.Contains(".winfo") && i<10)
                {
                    
                    string[] lines = System.IO.File.ReadAllLines(file.FullName);
                    string level = ConfigurationManager.AppSettings.Get("level_" + App.language) + lines[2].Split(':')[1];
                    string coins = ConfigurationManager.AppSettings.Get("coins_" + App.language) + lines[0].Split(':')[1];
                    string lifes = ConfigurationManager.AppSettings.Get("lifes_" + App.language) + lines[1].Split(':')[1];
                    this.Scene.EntityManager.Find("File_" + i).FindComponent<TextComponent>().Text = level + " " + coins + " " + lifes;

                    if (i == 0)
                    {
                        this.Scene.EntityManager.Find("SelectFile_0").FindComponent<SpriteRenderer>().IsVisible = true;
                    }

                    i++;
                    maxFiles++;
                }
            }

            if (maxFiles == 0)
            {
                this.Scene.EntityManager.Find("NoFile").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("noFile_" + App.language);
                this.Scene.EntityManager.Find("NoFile").FindComponent<TextRenderer2D>().IsVisible = true;
            }

            bloqued = true;
            this.Scene.EntityManager.Find("Back").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("back_" + App.language);
            var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
            {
                bloqued = false;
            }, false);
            position = 0;
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!this.isInitialized)
            {
                this.isInitialized = true;
                this.Initialize();
            }

            if (WaveServices.Input.KeyboardState.Enter == ButtonState.Pressed && !bloqued && maxFiles>0)
            {
                bloqued = true;

                var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    bloqued = false;
                }, false);

                this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                this.LoadGame();

                
            }
            else if ((WaveServices.Input.KeyboardState.Up == ButtonState.Pressed) && !bloqued)
            {
                if (position == 0)
                {
                    position = maxFiles-1;
                    this.Scene.EntityManager.Find("SelectFile_0").FindComponent<SpriteRenderer>().IsVisible = false;
                    this.Scene.EntityManager.Find("SelectFile_"+ position).FindComponent<SpriteRenderer>().IsVisible = true;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                }
                else
                {
                    this.Scene.EntityManager.Find("SelectFile_" + (position--)).FindComponent<SpriteRenderer>().IsVisible = false;
                    this.Scene.EntityManager.Find("SelectFile_" + position).FindComponent<SpriteRenderer>().IsVisible = true;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                }

                bloqued = true;

                var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    bloqued = false;
                }, false);
            }
            else if ((WaveServices.Input.KeyboardState.Down == ButtonState.Pressed) && !bloqued)
            {
                if (position == maxFiles - 1)
                {
                    position = 0;
                    this.Scene.EntityManager.Find("SelectFile_"+(maxFiles-1)).FindComponent<SpriteRenderer>().IsVisible = false;
                    this.Scene.EntityManager.Find("SelectFile_" + position).FindComponent<SpriteRenderer>().IsVisible = true;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                }
                else
                {
                    this.Scene.EntityManager.Find("SelectFile_" + (position++)).FindComponent<SpriteRenderer>().IsVisible = false;
                    this.Scene.EntityManager.Find("SelectFile_" + position).FindComponent<SpriteRenderer>().IsVisible = true;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                }

                bloqued = true;

                var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    bloqued = false;
                }, false);
            }
            else if ((WaveServices.Input.KeyboardState.B == ButtonState.Pressed) && !bloqued)
            {
                this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);
                var context = new ScreenContext(new MainScene());
                WaveServices.ScreenContextManager.To(context, transition);

                bloqued = true;

                var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    bloqued = false;
                }, false);
            }
        }

        private void LoadGame()
        {
            this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);

            DirectoryInfo di = new DirectoryInfo("../../../../../Content/savedGames");
            List<FileInfo> files = new List<FileInfo>();
            

            foreach (FileInfo f in di.GetFiles())
            {
                if (!f.Name.Contains(".winfo"))
                {
                    files.Add(f);
                }
            }

            FileInfo file = files[position];

            MapScene scene = new MapScene();
            scene.Level = System.IO.File.ReadAllLines(file.FullName)[2].Split(':')[1];
            App.savedGame = file.FullName;

            var context = new ScreenContext(scene);
            WaveServices.ScreenContextManager.To(context, transition);
        }
    }
}
