﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Math;
using WaveEngine.Components.Animation;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;
using WaveEngine.Framework.Graphics;

namespace GodsOfOlympus.Behaviors
{
    [DataContract]
    class FrogBehavior : Behavior
    {
        [RequiredComponent]
        public Transform2D Transform;

        [RequiredComponent]
        public Animation2D Animation;

        private const float Speed = 30;
        private Vector2 initPosition;

        private int limiteIzquierdo, limiteDerecho;
        

        private AnimState currentState, lastState;
        private enum AnimState { Idle, JumpRight, JumpLeft };

        private bool isInitialized = false;
        DateTime time;

        public FrogBehavior(int limiteIzquierdo, int limiteDerecho)
        {
            this.Transform = null;
            this.limiteDerecho = limiteDerecho;
            this.limiteIzquierdo = limiteIzquierdo;
            this.currentState = AnimState.Idle;
            this.lastState = AnimState.Idle;
        }

        protected override void Initialize()
        {
            base.Initialize();

            this.initPosition = this.Transform.Position;

            time = DateTime.Now;
        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!isInitialized)
            {
                this.isInitialized = true;
                this.Initialize();
            }
            if (limiteIzquierdo!= 0 && limiteDerecho!=0)
            {
                DateTime aux = DateTime.Now;
                TimeSpan result = aux.Subtract(time);
                int aux2 = result.Seconds;
                if (result.Seconds >= 3)
                {
                    WaveEngine.Framework.Services.Random r = new WaveEngine.Framework.Services.Random();
                    if (r.NextDouble() < 0.5)
                    {
                        float left = this.Transform.X - limiteIzquierdo;
                        float right = limiteDerecho - this.Transform.X;

                        if (left < right)
                        {
                            this.lastState = this.currentState;
                            this.currentState = AnimState.JumpRight;
                        }
                        else
                        {
                            this.lastState = this.currentState;
                            this.currentState = AnimState.JumpLeft;
                        }

                    }
                    else
                    {
                        this.lastState = this.currentState;
                        this.currentState = AnimState.Idle;
                        Animation.PlayAnimation("Idle", true);
                    }

                    time = DateTime.Now;
                }

                if (currentState != lastState)
                {
                    switch (currentState)
                    {
                        case AnimState.Idle:
                            Animation.PlayAnimation("Idle", true);
                            lastState = currentState;
                            break;
                        case AnimState.JumpLeft:
                            if (Animation.CurrentAnimation != "Jump")
                            {
                                Animation.PlayAnimation("Jump", true);
                            }
                            this.Transform.Effect = WaveEngine.Common.Graphics.SpriteEffects.None;
                            this.JumpLeft(gameTime);
                            break;
                        case AnimState.JumpRight:
                            if (Animation.CurrentAnimation != "Jump")
                            {
                                Animation.PlayAnimation("Jump", true);
                            }
                            this.Transform.Effect = WaveEngine.Common.Graphics.SpriteEffects.FlipHorizontally;
                            this.JumpRight(gameTime);
                            break;
                    }
                }
            }
        }
        
        
        private void JumpRight(TimeSpan gameTime)
        {
            var localPosition = this.Transform.LocalPosition;
            if (limiteDerecho>this.Transform.X)
            {

                localPosition.X += (float)(Speed * gameTime.TotalSeconds);
                this.Transform.LocalPosition = localPosition;

            }
            else
            {
                this.lastState = this.currentState;
                this.currentState = AnimState.Idle;
            }
            

        }

        private void JumpLeft(TimeSpan gameTime)
        {
            var localPosition = this.Transform.LocalPosition;
            if (limiteIzquierdo < this.Transform.X)
            {

                localPosition.X -= (float)(Speed * gameTime.TotalSeconds);
                this.Transform.LocalPosition = localPosition;
                
            }
            else
            {
                this.lastState = this.currentState;
                this.currentState = AnimState.Idle;
            }
        }
    }
}
