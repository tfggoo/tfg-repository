﻿using GodsOfOlympus.Components;
using GodsOfOlympus.Scenes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Input;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Components.Toolkit;
using WaveEngine.Components.Transitions;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Behaviors
{
    class MainSceneBehavior : SceneBehavior
    {
        private ScreenTransition transition;
        private static readonly TimeSpan TRANSITIONTIME = new TimeSpan(0, 0, 0, 1, 0);
        private bool isInitialized = false;
        private bool bloqued = false;
        private string option;

        protected override void ResolveDependencies()
        {
            
        }

        private void Initialize()
        {
            option = "new";

        }

        protected override void Update(TimeSpan gameTime)
        {
            if (!this.isInitialized)
            {
                this.isInitialized = true;
                this.Initialize();
            }

            if (WaveServices.Input.KeyboardState.Enter == ButtonState.Pressed && !bloqued)
            {
                if (option == "new")
                {
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                    this.NewGame();
                }
                else
                {
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                    this.LoadGame();
                }

                bloqued = true;

                var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    bloqued = false;
                }, false);
            }
            else if ((WaveServices.Input.KeyboardState.Up == ButtonState.Pressed || WaveServices.Input.KeyboardState.Down == ButtonState.Pressed) && !bloqued)
            {
                if (option == "new")
                {
                    option = "load";
                    this.Scene.EntityManager.Find("SelectNew").FindComponent<SpriteRenderer>().IsVisible = false;
                    this.Scene.EntityManager.Find("SelectLoad").FindComponent<SpriteRenderer>().IsVisible = true;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                }
                else
                {
                    option = "new";
                    this.Scene.EntityManager.Find("SelectNew").FindComponent<SpriteRenderer>().IsVisible = true;
                    this.Scene.EntityManager.Find("SelectLoad").FindComponent<SpriteRenderer>().IsVisible = false;
                    this.Scene.EntityManager.Find("SoundManager").FindComponent<SoundComponent>().PlaySound(SoundType.Menu);
                }

                bloqued = true;

                var timer = WaveServices.TimerFactory.CreateTimer(TimeSpan.FromSeconds(0.25), () =>
                {
                    bloqued = false;
                }, false);
            }
        }

        private void NewGame()
        {
            this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);

            MapScene sceneMap = new MapScene();
            sceneMap.Level = "1";
            var context = new ScreenContext(sceneMap);
            WaveServices.ScreenContextManager.To(context, transition);
        }

        private void LoadGame()
        {
            this.transition = new ColorFadeTransition(Color.White, TRANSITIONTIME);
            
            var context = new ScreenContext(new LoadGameScene());
            WaveServices.ScreenContextManager.To(context, transition);
        }
    }
}
