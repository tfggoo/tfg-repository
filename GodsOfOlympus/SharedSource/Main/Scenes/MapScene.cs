﻿using GodsOfOlympus.Behaviors;
using System;
using System.Collections.Generic;
using System.Text;
using WaveEngine.Framework;

namespace GodsOfOlympus.Scenes
{
    class MapScene : Scene
    {
        private string level;

        public string Level
        {
            get
            {
                return this.level;
            }
            set
            {
                this.level = value;
            }
        }

        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.MapScene);
        }

        protected override void Start()
        {
            base.Start();
            this.AddSceneBehavior(new MapSceneBehavior(level), SceneBehavior.Order.PostUpdate);
        }
    }
}
