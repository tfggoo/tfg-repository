﻿using GodsOfOlympus.Behaviors;
using System;
using System.Collections.Generic;
using System.Text;
using WaveEngine.Framework;

namespace GodsOfOlympus.Scenes
{
    class WinScene : Scene
    {
        private string level;
        private string coins;
        private string collectedCoins;

        public string Level
        {
            get
            {
                return this.level;
            }
            set
            {
                this.level = value;
            }
        }

        public string Coins
        {
            get
            {
                return this.coins;
            }
            set
            {
                this.coins = value;
            }
        }

        public string CollectedCoins
        {
            get
            {
                return this.collectedCoins;
            }
            set
            {
                this.collectedCoins = value;
            }
        }

        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.WinScene);
        }

        protected override void Start()
        {
            base.Start();
            this.AddSceneBehavior(new FinishSceneBehavior(level, coins , collectedCoins), SceneBehavior.Order.PostUpdate);
        }
    }
}
