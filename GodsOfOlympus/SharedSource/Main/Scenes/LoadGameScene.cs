﻿using GodsOfOlympus.Behaviors;
using System;
using System.Collections.Generic;
using System.Text;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Scenes
{
    class LoadGameScene : Scene
    {
        
        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.LoadGameScene);
        }

        protected override void Start()
        {
            base.Start();
            this.AddSceneBehavior(new LoadGameSceneBehavior(), SceneBehavior.Order.PostUpdate);
        }
    }
}
