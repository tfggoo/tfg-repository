﻿using GodsOfOlympus.Behaviors;
using GodsOfOlympus.Components;
using System;
using System.Collections.Generic;
using System.Text;
using WaveEngine.Common.Math;
using WaveEngine.Common.Media;
using WaveEngine.Components.Animation;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;
using WaveEngine.Framework.Services;
using WaveEngine.TiledMap;

namespace GodsOfOlympus.Scenes
{
    class FourthLevelScene : Scene
    {
        private WaveEngine.TiledMap.TiledMap tiledMap;
        private static int CrateTileID = 190;
        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.FourthLevelScene);
        }

        protected override void Start()
        {
            base.Start();

            this.tiledMap = this.EntityManager.Find("FourthLevel").FindComponent<WaveEngine.TiledMap.TiledMap>();


            this.AddSceneColliders();
            this.AddEnd();
            this.AddTraps();
            this.StartMusic();
            this.AddCoins();
            this.AddSpecialCoins();
            this.AddEnemies();
            this.AddSceneBehavior(new FourthLevelSceneBehavior(), SceneBehavior.Order.PostUpdate);
            this.AddLifes();
            if (App.savedGame != null)
            {
                this.LoadDependencesLevel();
            }
        }

        private void LoadDependencesLevel()
        {
            string[] lines = System.IO.File.ReadAllLines(App.savedGame);
            this.EntityManager.Find("Camera2D").FindComponent<CharacterAttributes>().Lifes = Int32.Parse(lines[1].Split(':')[1]);
        }

        private void AddTraps()
        {
            var collisionLayer = this.tiledMap.ObjectLayers["Traps"];

            int i = 0;
            foreach (var obj in collisionLayer.Objects)
            {
                var colliderEntity = TiledMapUtils.CollisionEntityFromObject("trap_" + (i++), obj);
                colliderEntity.Tag = "trap";

                this.EntityManager.Add(colliderEntity);
            }
        }

        private void AddEnd()
        {
            var endLayer = this.tiledMap.ObjectLayers["End"];
            var endObj = endLayer.Objects[0];
            var endEntity = TiledMapUtils.CollisionEntityFromObject("End", endObj);

            this.EntityManager.Add(endEntity);
        }

        private void AddSceneColliders()
        {
            var collisionLayer = this.tiledMap.ObjectLayers["Collisions"];

            int i = 0;
            foreach (var obj in collisionLayer.Objects)
            {
                var colliderEntity = TiledMapUtils.CollisionEntityFromObject("collider_" + (i++), obj);
                colliderEntity.Tag = "collider";

                colliderEntity.AddComponent(new RigidBody2D() { PhysicBodyType = WaveEngine.Common.Physics2D.RigidBodyType2D.Static });

                this.EntityManager.Add(colliderEntity);
            }
        }

        private void AddSpecialCoins()
        {
            var cratesLayer = this.tiledMap.ObjectLayers["SpecialCoins"];

            int i = 0;
            foreach (var obj in cratesLayer.Objects)
            {
                Entity crateEntity = new Entity("specialcoin_" + (i++)) { Tag = "specialcoin" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(obj.X, obj.Y), Origin = Vector2.Center, DrawOrder = -9 })
                .AddComponent(new SpriteAtlas(WaveContent.Assets.Animations.SpecialCoin_spritesheet))
                .AddComponent(new Animation2D() { PlayAutomatically = true, CurrentAnimation = "Flip" })
                //.AddComponent(new SpriteAtlasRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteAtlasRenderer())
                .AddComponent(new CircleCollider2D())
                ;

                this.EntityManager.Add(crateEntity);
            }
        }

        private void AddCoins()
        {
            var cratesLayer = this.tiledMap.ObjectLayers["Coins"];

            int i = 0;
            foreach (var obj in cratesLayer.Objects)
            {
                Entity crateEntity = new Entity("coin_" + (i++)) { Tag = "coin" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(obj.X, obj.Y), Origin = Vector2.Center, DrawOrder = -100 })
                .AddComponent(new SpriteAtlas(WaveContent.Assets.Animations.Coin_spritesheet))
                .AddComponent(new Animation2D() { PlayAutomatically = true, CurrentAnimation = "Flip" })
                //.AddComponent(new SpriteAtlasRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteAtlasRenderer())
                .AddComponent(new CircleCollider2D())
                ;

                this.EntityManager.Add(crateEntity);
            }
        }

        private void AddEnemies()
        {
            var eaglesLayer = this.tiledMap.ObjectLayers["Eagles"];
            int i = 0;
            foreach (var obj in eaglesLayer.Objects)
            {
                var entity = new Entity("eagle_" + (i++))
                .AddComponent(new Transform2D() { X = obj.X, Y = obj.Y, LocalDrawOrder = -200.0F, Scale = new Vector2(1.0F, 1.0F) })
                .AddComponent(new SpriteAtlas(WaveContent.Assets.Animations.Eagle_spritesheet))
                .AddComponent(new SpriteAtlasRenderer())
                .AddComponent(new Animation2D() { CurrentAnimation = "Fly", PlayAutomatically = true })
                .AddComponent(new RectangleCollider2D())
                .AddComponent(new EagleBehavior());

                entity.Tag = "Enemy";
                this.EntityManager.Add(entity);
            }
        }

        private void StartMusic()
        {
            MusicInfo musicInfo = new MusicInfo(WaveContent.Assets.Sounds.ff9Battle_mp3);
            WaveServices.MusicPlayer.IsRepeat = true;
            WaveServices.MusicPlayer.Volume = 0.6f;
            WaveServices.MusicPlayer.Play(musicInfo);
        }
        private void AddLifes()
        {
            var collisionLayer = this.tiledMap.ObjectLayers["Lifes"];

            int i = 0;
            foreach (var obj in collisionLayer.Objects)
            {
                Entity lifeEntity = new Entity("life_" + (i++)) { Tag = "life" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(obj.X, obj.Y), Origin = Vector2.Center, DrawOrder = -9 })
                .AddComponent(new Sprite(WaveContent.Assets.Textures.heart_full_16x16_png))
                //.AddComponent(new SpriteRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteRenderer())
                .AddComponent(new CircleCollider2D())
                ;

                this.EntityManager.Add(lifeEntity);
            }
        }
    }
}
