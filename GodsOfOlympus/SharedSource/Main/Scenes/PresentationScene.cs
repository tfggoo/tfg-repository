﻿using GodsOfOlympus.Behaviors;
using System;
using System.Collections.Generic;
using System.Text;
using WaveEngine.Framework;

namespace GodsOfOlympus.Scenes
{
    class PresentationScene : Scene
    {
        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.PresentationScene);
        }

        protected override void Start()
        {
            base.Start();
            this.AddSceneBehavior(new PresentationSceneBehavior(), SceneBehavior.Order.PostUpdate);
        }
    }
}
