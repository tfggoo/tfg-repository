﻿using GodsOfOlympus.Behaviors;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using WaveEngine.Common.Media;
using WaveEngine.Components.Toolkit;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Scenes
{
    class GameOverScene : Scene
    {
        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.GameOverScene);
        }

        protected override void Start()
        {
            base.Start();

            this.AddSceneBehavior(new FinishSceneBehavior("gameover", "gameover", "gameover"), SceneBehavior.Order.PostUpdate);
            this.EntityManager.Find("GameOver").FindComponent<TextComponent>().Text = string.Format(ConfigurationManager.AppSettings.Get("gameOver_" + App.language), Environment.NewLine);
            this.StartMusic();
        }

        private void StartMusic()
        {
            MusicInfo musicInfo = new MusicInfo(WaveContent.Assets.Sounds.Snowland_mp3);
            WaveServices.MusicPlayer.IsRepeat = true;
            WaveServices.MusicPlayer.Volume = 0.6f;
            WaveServices.MusicPlayer.Play(musicInfo);
        }
    }
}
