﻿using GodsOfOlympus.Behaviors;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using WaveEngine.Common.Media;
using WaveEngine.Components.Toolkit;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;

namespace GodsOfOlympus.Scenes
{
    class MainScene : Scene
    {
        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.MainScene);
        }

        protected override void Start()
        {
            base.Start();

            this.AddSceneBehavior(new MainSceneBehavior(), SceneBehavior.Order.PostUpdate);
            this.StartMusic();
            this.EntityManager.Find("NewGame").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("newGame_" + App.language);
            this.EntityManager.Find("LoadGame").FindComponent<TextComponent>().Text = ConfigurationManager.AppSettings.Get("loadGame_" + App.language);
        }

        private void StartMusic()
        {
            MusicInfo musicInfo = new MusicInfo(WaveContent.Assets.Sounds.Snowland_mp3);
            WaveServices.MusicPlayer.IsRepeat = true;
            WaveServices.MusicPlayer.Volume = 0.6f;
            WaveServices.MusicPlayer.Play(musicInfo);
        }
    }
}
