#region Using Statements
using GodsOfOlympus.Behaviors;
using System;
using System.Collections.Generic;
using WaveEngine.Common;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Math;
using WaveEngine.Common.Media;
using WaveEngine.Components.Animation;
using WaveEngine.Components.Cameras;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Components.Graphics3D;
using WaveEngine.Components.Toolkit;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics2D;
using WaveEngine.Framework.Resources;
using WaveEngine.Framework.Services;
using WaveEngine.TiledMap;
#endregion

namespace GodsOfOlympus
{
    public class MyScene : Scene
    {
        private WaveEngine.TiledMap.TiledMap tiledMap;
        private static int CrateTileID = 190;
        private List<Entity> frogs;

        protected override void CreateScene()
        {
            this.Load(WaveContent.Scenes.MyScene);
        }

        protected override void Start()
        {
            base.Start();

            this.tiledMap = this.EntityManager.Find("FirstLevel").FindComponent<WaveEngine.TiledMap.TiledMap>();
           

            this.AddSceneColliders();
            this.AddEnd();
            this.AddTraps();
            this.AddStairs();
            this.AddCoins();
            this.AddSpecialCoins();
            this.StartMusic();
            this.AddEnemies();
            this.AddLifes();
            this.AddSceneBehavior(new GameplaySceneBehavior(), SceneBehavior.Order.PostUpdate);
        }

        private void AddSpecialCoins()
        {
            var cratesLayer = this.tiledMap.ObjectLayers["SpecialCoins"];

            int i = 0;
            foreach (var obj in cratesLayer.Objects)
            {
                Entity crateEntity = new Entity("specialcoin_" + (i++)) { Tag = "specialcoin" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(obj.X, obj.Y), Origin = Vector2.Center, DrawOrder = -9 })
                .AddComponent(new SpriteAtlas(WaveContent.Assets.Animations.SpecialCoin_spritesheet))
                .AddComponent(new Animation2D() { PlayAutomatically = true, CurrentAnimation = "Flip" })
                //.AddComponent(new SpriteAtlasRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteAtlasRenderer())
                .AddComponent(new CircleCollider2D())
                ;

                this.EntityManager.Add(crateEntity);
            }
        }

        private void AddEnd()
        {
            var endLayer = this.tiledMap.ObjectLayers["End"];
            var endObj = endLayer.Objects[0];
            var endEntity = TiledMapUtils.CollisionEntityFromObject("End", endObj);

            this.EntityManager.Add(endEntity);
        }

        private void AddCoins()
        {
            var cratesLayer = this.tiledMap.ObjectLayers["Coins"];

            int i = 0;
            foreach (var obj in cratesLayer.Objects)
            {
                Entity crateEntity = new Entity("coin_" + (i++)) { Tag = "coin" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(obj.X, obj.Y), Origin = Vector2.Center, DrawOrder = -9 })
                .AddComponent(new SpriteAtlas(WaveContent.Assets.Animations.Coin_spritesheet))
                .AddComponent(new Animation2D() { PlayAutomatically = true, CurrentAnimation = "Flip" })
                //.AddComponent(new SpriteAtlasRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteAtlasRenderer())
                .AddComponent(new CircleCollider2D())
                ;

                this.EntityManager.Add(crateEntity);
            }
        }

        private void AddStairs()
        {
            var collisionLayer = this.tiledMap.ObjectLayers["Stairs"];

            int i = 0;
            foreach (var obj in collisionLayer.Objects)
            {
                var colliderEntity = TiledMapUtils.CollisionEntityFromObject("stair_" + (i++), obj);
                colliderEntity.Tag = "stair";

                this.EntityManager.Add(colliderEntity);
            }
        }

        private void AddTraps()
        {
            var collisionLayer = this.tiledMap.ObjectLayers["Traps"];

            int i = 0;
            foreach (var obj in collisionLayer.Objects)
            {
                var colliderEntity = TiledMapUtils.CollisionEntityFromObject("trap_" + (i++), obj);
                colliderEntity.Tag = "trap";

                this.EntityManager.Add(colliderEntity);
            }
        }

        private void AddSceneColliders()
        {
            var collisionLayer = this.tiledMap.ObjectLayers["Collisions"];

            int i = 0;
            foreach (var obj in collisionLayer.Objects)
            {
                var colliderEntity = TiledMapUtils.CollisionEntityFromObject("collider_" + (i++), obj);
                colliderEntity.Tag = "collider";

                colliderEntity.AddComponent(new RigidBody2D() { PhysicBodyType = WaveEngine.Common.Physics2D.RigidBodyType2D.Static });

                this.EntityManager.Add(colliderEntity);
            }
        }

        private void StartMusic()
        {
            MusicInfo musicInfo = new MusicInfo(WaveContent.Assets.Sounds.ff9Battle_mp3);
            WaveServices.MusicPlayer.IsRepeat = true;
            WaveServices.MusicPlayer.Volume = 0.6f;
            WaveServices.MusicPlayer.Play(musicInfo);
        }

        private void AddEnemies()
        {
            var eaglesLayer = this.tiledMap.ObjectLayers["Eagles"];
            int i = 0;
            foreach (var obj in eaglesLayer.Objects)
            {
                var entity = new Entity("eagle_"+(i++))
                .AddComponent(new Transform2D() {X = obj.X, Y=obj.Y, LocalDrawOrder = -200.0F, Scale = new Vector2(0.5F,0.5F)})
                .AddComponent(new SpriteAtlas(WaveContent.Assets.Animations.Eagle_spritesheet))
                .AddComponent(new SpriteAtlasRenderer())
                .AddComponent(new Animation2D() {CurrentAnimation = "Fly",PlayAutomatically = true})
                .AddComponent(new RectangleCollider2D())
                .AddComponent(new EagleBehavior());

                entity.Tag = "Enemy";
                this.EntityManager.Add(entity);
            }

            var frogsLayer = this.tiledMap.ObjectLayers["Frogs"];
            var frogsProperties = this.tiledMap.ObjectLayers["Frogs"].Properties;
            int j = 0;

            frogs = new List<Entity>();
            foreach (var obj in frogsLayer.Objects)
            {
                int limiteIzquierdo = 0;
                int limiteDerecho = 0;
                var x = 0;
                foreach(var pro in frogsProperties)
                {
                    if (x == j)
                    {
                        char separador = ',';
                        string[] cadenas = pro.ToString().Split(separador);
                        limiteIzquierdo = int.Parse(cadenas[1]);
                        char separador2 = ']';
                        string[] subcadena = cadenas[2].Split(separador2);
                        limiteDerecho = int.Parse(subcadena[0]);
                        break;
                    }
                    x++;

                }
                
                var entity = new Entity("frog_" + (j++))
                .AddComponent(new Transform2D() { X = obj.X, Y = obj.Y, LocalDrawOrder = -200.0F, Scale = new Vector2(0.5F, 0.5F) })
                .AddComponent(new SpriteAtlas(WaveContent.Assets.Animations.Frog_spritesheet))
                .AddComponent(new SpriteAtlasRenderer())
                .AddComponent(new Animation2D() { CurrentAnimation = "Idle", PlayAutomatically = true })
                .AddComponent(new RectangleCollider2D() { Friction = 1})
                .AddComponent(new FrogBehavior(limiteIzquierdo,limiteDerecho));

                entity.Tag = "Enemy";
                this.EntityManager.Add(entity);
                this.frogs.Add(entity);
            }
        }

        private void AddLifes()
        {
            var collisionLayer = this.tiledMap.ObjectLayers["Lifes"];

            int i = 0;
            foreach (var obj in collisionLayer.Objects)
            {
                Entity lifeEntity = new Entity("life_" + (i++)) { Tag = "life" }
                .AddComponent(new Transform2D() { LocalPosition = new Vector2(obj.X, obj.Y), Origin = Vector2.Center, DrawOrder = -9 })
                .AddComponent(new Sprite(WaveContent.Assets.Textures.heart_full_16x16_png))
                //.AddComponent(new SpriteRenderer(DefaultLayers.Alpha, AddressMode.PointClamp))
                .AddComponent(new SpriteRenderer())
                .AddComponent(new CircleCollider2D())
                ;

                this.EntityManager.Add(lifeEntity);
            }
        }
    }
}
